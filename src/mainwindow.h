#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QPrinter>
#include <QShortcut>
#include <QPrintDialog>
#include <QPrintPreviewDialog>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_load();

    void on_reload();

    void on_print();

    void on_print_preview(QPrinter*);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
