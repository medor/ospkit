#include <QApplication>
#include <QWebPage>
#include <QWebFrame>
#include <QTimer>
#include <QFileInfo>
#include <QPrinter>
#include <QCommandLineParser>
#include <QDebug>

class Task : public QObject {
    Q_OBJECT
public:
    Task(QObject *parent = 0) : QObject(parent) {}

public slots:
    void run()
    {
        //QUrl url = QUrl("http://localhost:8001/content/flat-flatland.html");
        //QUrl url = QUrl("http://localhost:8001/layout/autopages/index.html");
        QUrl url = QUrl("http://lemonde.fr");

        connect(&page, SIGNAL(loadFinished(bool)), this, SLOT(wait()));
        page.mainFrame()->load(url);
    }

    void wait()
    {
        QTimer *timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(print()));
        timer->start(1000);
    }

    void print()
    {
        QPrinter printer;
        printer.setPrinterName("PDFCreator");
        printer.setPaperSize(QPrinter::A4);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName("output.pdf");

        page.mainFrame()->print(&printer);

        emit finished();
    }

signals:
    void finished();

private:
    QWebPage page;
};

#include "main.moc"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setApplicationName("ospkit");
    QCoreApplication::setApplicationVersion("0.1");

    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("source", QCoreApplication::translate("main", "Source file to copy."));
//    parser.addPositionalArgument("destination", QCoreApplication::translate("main", "Destination directory."));

    // Process the actual command line arguments given by the user
    parser.process(app);

    const QStringList args = parser.positionalArguments();
    // source is args.at(0), destination is args.at(1)

    qDebug() << args.at(0);

    // Task parented to the application so that it
    // will be deleted by the application.
    Task *task = new Task(&app);

    // This will cause the application to exit when
    // the task signals finished.
    QObject::connect(task, SIGNAL(finished()), &app, SLOT(quit()));

    // This will run the task from the application event loop.
    QTimer::singleShot(0, task, SLOT(run()));

    return app.exec();
}
