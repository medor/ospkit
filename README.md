OSPKit
======

This browser is meant to be used with the project html2print available here: <http://osp.kitchen/tools/html2print/>.
The aim is to lay out printed documents within a web browser.
We built our own webkit browser in order to have a faster browser and good typography (weirdly, the bearings and kernings can be weird in certain webkit browsers).

For Ubuntu user, before compilation
-----------------------------------

    sudo apt install qt5-default libqt5webkit5-dev ruby-dev qtpositioning5-dev
    sudo apt install libqt5sensors5-dev libxcomposite-dev libglib2.0-dev libxrender-dev
    

Download 
--------

Download the zip file and decompress it (or git clone it), then go into the directory
    
    cd tools.ospkit


Compile
--------------
    
    cd src
    qmake
    make


Launch the app
---------------------

From the WebkitApp directory:

    ./OSPKit


Available shortcuts
-------------------

- Ctrl + P: Print to file
- Ctrl + Shift + P: Print dialog (allows output page resizing)
- Ctrl + R: Reload

Using our patched version of QTWebkit
-------------------------------------

See <https://github.com/annulen/webkit/wiki>

    sudo apt-get install build-essential perl python ruby flex gperf bison qtbase5-private-dev

    cmake ninja-build libfontconfig1-dev libicu-dev libsqlite3-dev
    
    cmake zlib1g-dev libpng12-dev libjpeg-dev libxslt1-dev libxml2-dev libhyphen-dev
    
    cd ../..

Git clone our version of webkit with specifying only the last version with '--depth 1' (if not, it's heavy, around 6,3GB as it is 3 millions of files!)
    
    git clone -b ospkit --depth 1 --single-branch https://github.com/aleray/webkit.git
    
Compile. It is a heavy operation, fans will work on you computer...

    cd webkit
    
    WEBKIT_OUTPUTDIR=`pwd`/build/qt
    
    Tools/Scripts/build-webkit --qt --release --no-web-audio --no-video
    
    cd WebKitBuild/qt/Release
    
    sudo ninja install
    
Run OSPKit with this newly-built code
------------------------------------------

     cd ..
     
     ./OSPKit

Is it working? If not, send us feedback on miam@osp.kitchen (we will try to help if possible). If yes, a blank window is still not really exciting :) but we are preparing a boilerplate to start really to use it! 
