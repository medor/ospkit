#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->webView->page()->settings()->setAttribute(QWebSettings::DeveloperExtrasEnabled, true);
    ui->webView->page()->settings()->setAttribute(QWebSettings::CSSGridLayoutEnabled, true);
    ui->webView->page()->settings()->setAttribute(QWebSettings::LocalStorageEnabled, true);
    ui->webView->page()->settings()->setAttribute(QWebSettings::AcceleratedCompositingEnabled, true);

    QObject::connect( ui->loadButton, SIGNAL(clicked()), this, SLOT( on_load() ) );
    QObject::connect( ui->urlEdit, SIGNAL(returnPressed()), this, SLOT( on_load() ) );

    QShortcut *printShortcut = new QShortcut(QKeySequence("Ctrl+P"), this );
    QObject::connect( printShortcut, SIGNAL(activated()), this, SLOT( on_print() ) );
    QObject::connect( ui->printButton, SIGNAL(clicked()), this, SLOT( on_print() ) );

    QShortcut *reloadShortcut = new QShortcut(QKeySequence("Ctrl+R"), this );
    QObject::connect( reloadShortcut, SIGNAL(activated()), this, SLOT( on_reload() ) );
    QObject::connect( ui->reloadButton, SIGNAL(clicked()), this, SLOT( on_reload() ) );

    QObject::connect( ui->webView->page(), SIGNAL(printRequested(QWebFrame *)), this, SLOT(on_print()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_load()
{
    ui->webView->load((ui->urlEdit->text()));
}


void MainWindow::on_reload()
{
    ui->webView->reload();
}


void MainWindow::on_print_preview(QPrinter *printer)
{
    ui->webView->print( printer );
}

void MainWindow::on_print()
{
    QPrinter printer;
    QSizeF papersize( 578, 747 );

    printer.setOutputFormat( QPrinter::PdfFormat );
    printer.setPrintRange( QPrinter::AllPages );
    printer.setResolution( 300 );
    printer.setPageMargins( 0,0,0,0, QPrinter::Point );
    printer.setPaperSize( papersize, QPrinter::Point );
    printer.setFullPage( true );
    printer.setFontEmbeddingEnabled( true );
//    printer.setOutputFileName( filename );

//    QPrintDialog *dialog = new QPrintDialog(&printer);
//    if ( dialog->exec() == QDialog::Accepted)
//        ui->webView->print( &printer );

//    QPrintDialog printDlg(&printer);
//    if (printDlg.exec() == QDialog::Rejected)
//        return;

//    ui->webView->print( &printer );


    QPrintPreviewDialog preview(&printer);
    preview.adjustSize();
    preview.setMinimumSize(800, 600);
    preview.setWindowTitle("Test imp");

    connect(&preview, SIGNAL(paintRequested(QPrinter*)), this, SLOT(on_print_preview(QPrinter*)));

    preview.exec();
}

