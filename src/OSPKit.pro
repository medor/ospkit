    #-------------------------------------------------
    #
    # Project created by QtCreator 2016-06-08T00:53:49
    #
    #-------------------------------------------------

    WEBKITPATH = /usr/local
    LIBS += -L$$WEBKITPATH/lib -lQt5WebKit -lQt5WebKitWidgets -Wl,-rpath,$$WEBKITPATH/lib
    INCLUDEPATH += $$WEBKITPATH/include $$WEBKITPATH/include/QtWebKit $$WEBKITPATH/include/QtWebKitWidgets


    QT       += core gui printsupport
    #QT       += webkitwidgets

    greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

    TARGET = OSPKit
    TEMPLATE = app


    SOURCES += main.cpp\
            mainwindow.cpp

    HEADERS  += mainwindow.h

    FORMS    += mainwindow.ui
